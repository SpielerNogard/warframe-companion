import requests
import json
from HtmlGenerator import HtmlGenerator

def read_droptable(Search):
        
        Droptable = []
        #print(Search)
        url='https://api.warframestat.us/drops/search/'+Search
        response = requests.get(url, stream=True)
        #print(str(response))
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["place"]
                Out2 = python_json_obj["item"]
                Out3 = python_json_obj["rarity"]
                Out5 = python_json_obj["chance"]
                
                Out = "Ort: "+str(Out1)+" , Item: "+str(Out2)+" , Seltenheit: "+str(Out3)+" , Chance: "+str(Out5)+"% "
                #print(Out)  
                Ding = (Out1,Out2,Out3,Out5)
                Droptable.append(Ding)
                

            except:
                info="Fehler in fissures PC"
                print(info)
        #for j in Droptable:
            #print(str(j))
        
        return(Droptable)

def ordne_droptable(Search):
    Ding = read_droptable(Search)
    #print(Ding)
    a= sorted(Ding, key=lambda student: student[3])
    #print(a) 
    #a = a.reverse() 
    #for i in a:
        #print(i)
    a.reverse()
    #for i in a:
        #print(i)
    return(a)

def vollständige_suche(Search):
    a = ordne_droptable(Search)
    HTML = HtmlGenerator()
    HTML.new_line("Hier ist die Droptable für dein Item")
    for i in a:
        Out1 = i[0]
        Out2 = i[1]
        Out3 = i[2]
        Out5 = i[3]
                
        Out = "Ort: "+str(Out1)+" , Item: "+str(Out2)+" , Seltenheit: "+str(Out3)+" , Chance: "+str(Out5)+"% "
        print(Out)
        HTML.new_line(Out)  
    HTML.new_line("______________________________________________________________________")
    text = HTML.give_html("1")
    #print(text)
    return(text)

#vollständige_suche("Axi W1")
#ordne_droptable()
#read_droptable("Axi W1")
#test()