import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import FunktionenBot
from PyQt5 import QtCore, QtGui, QtWidgets
from HtmlGenerator import HtmlGenerator
import DroptableReader
class ScrollLabel(QScrollArea): 
  
    # contructor 
    def __init__(self, *args, **kwargs): 
        QScrollArea.__init__(self, *args, **kwargs) 
  
        # making widget resizable 
        self.setWidgetResizable(True) 
  
        # making qwidget object 
        content = QWidget(self) 
        self.setWidget(content) 
  
        # vertical box layout 
        lay = QVBoxLayout(content) 
  
        # creating label 
        self.label = QLabel(content) 
  
        # setting alignment to the text 
        self.label.setAlignment(Qt.AlignLeft | Qt.AlignTop) 
  
        # making label multi-line 
        self.label.setWordWrap(True) 
  
        # adding label to the layout 
        lay.addWidget(self.label) 
  
    # the setText method 
    def setText(self, text): 
        # setting text to the label 
        self.label.setText(text) 
class WarframeCompanion(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initMe()
    
    def initMe(self):
        self.setMouseTracking(True)
        self.statusBar().showMessage('Warframe Companion V0.0.4')
        self.setGeometry(50,50,1000,500)
        self.setWindowTitle("Warframe Companion")
        self.setWindowIcon(QIcon("icon.jpg"))
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(0, 0, 7817, 1055))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.pushButton_7 = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.pushButton_7.setObjectName("pushButton_7")
        self.verticalLayout.addWidget(self.pushButton_7)
        self.pushButton = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.pushButton.setObjectName("pushButton")
        self.verticalLayout.addWidget(self.pushButton)
        self.pushButton_3 = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.pushButton_3.setObjectName("pushButton_3")
        self.verticalLayout.addWidget(self.pushButton_3)
        self.pushButton_4 = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.pushButton_4.setObjectName("pushButton_4")
        self.verticalLayout.addWidget(self.pushButton_4)
        self.pushButton_5 = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.pushButton_5.setObjectName("pushButton_5")
        self.verticalLayout.addWidget(self.pushButton_5)
        self.pushButton_6 = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.pushButton_6.setObjectName("pushButton_6")
        self.verticalLayout.addWidget(self.pushButton_6)
        self.pushButton_8 = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.pushButton_8.setObjectName("pushButton_8")
        self.verticalLayout.addWidget(self.pushButton_8)
        self.pushButton_9 = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.pushButton_9.setObjectName("pushButton_9")
        self.verticalLayout.addWidget(self.pushButton_9)
        self.pushButton_10 = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.pushButton_10.setObjectName("pushButton_10")
        self.verticalLayout.addWidget(self.pushButton_10)
        self.pushButton_11 = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.pushButton_11.setObjectName("pushButton_11")
        self.verticalLayout.addWidget(self.pushButton_11)
        self.pushButton_12 = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.pushButton_12.setObjectName("pushButton_12")
        self.verticalLayout.addWidget(self.pushButton_12)
        self.pushButton_13 = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.pushButton_13.setObjectName("pushButton_13")
        self.verticalLayout.addWidget(self.pushButton_13)
        self.pushButton_2 = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.pushButton_2.setObjectName("pushButton_2")
        self.verticalLayout.addWidget(self.pushButton_2)
        self.pushButton_16 = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.pushButton_16.setObjectName("pushButton_16")
        self.verticalLayout.addWidget(self.pushButton_16)

        self.pushButton_17 = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.pushButton_17.setObjectName("pushButton_17")
        self.verticalLayout.addWidget(self.pushButton_17)

        self.pushButton_18 = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.pushButton_18.setObjectName("pushButton_18")
        self.verticalLayout.addWidget(self.pushButton_18)

        self.pushButton_19 = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.pushButton_19.setObjectName("pushButton_19")
        self.verticalLayout.addWidget(self.pushButton_19)

        self.pushButton_21 = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.pushButton_21.setObjectName("pushButton_21")
        self.verticalLayout.addWidget(self.pushButton_21)
       
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)

        
        self.horizontalLayout.addLayout(self.verticalLayout)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(self)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1194, 21))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuSettings = QtWidgets.QMenu(self.menubar)
        self.menuSettings.setObjectName("menuSettings")
        self.menuArbitrations = QtWidgets.QMenu(self.menubar)
        self.menuArbitrations.setObjectName("menuArbitrations")
        self.menuFissures = QtWidgets.QMenu(self.menubar)
        self.menuFissures.setObjectName("menuFissures")
        self.menuVoid_Trader = QtWidgets.QMenu(self.menubar)
        self.menuVoid_Trader.setObjectName("menuVoid_Trader")
        self.setMenuBar(self.menubar)

        self.actionExit = QtWidgets.QAction(self)
        self.actionExit.setObjectName("actionExit")
        self.actionPC = QtWidgets.QAction(self)
        self.actionPC.setObjectName("actionPC")
        self.actionSwitch = QtWidgets.QAction(self)
        self.actionSwitch.setObjectName("actionSwitch")
        self.actionPlaystation_4 = QtWidgets.QAction(self)
        self.actionPlaystation_4.setObjectName("actionPlaystation_4")
        self.actionXbox_One = QtWidgets.QAction(self)
        self.actionXbox_One.setObjectName("actionXbox_One")
        self.menuFile.addAction(self.actionExit)
        self.menuArbitrations.addAction(self.actionPC)
        self.menuArbitrations.addSeparator()
        self.menuArbitrations.addAction(self.actionSwitch)
        self.menuArbitrations.addSeparator()
        self.menuArbitrations.addAction(self.actionPlaystation_4)
        self.menuArbitrations.addSeparator()
        self.menuArbitrations.addAction(self.actionXbox_One)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuSettings.menuAction())
        self.menubar.addAction(self.menuArbitrations.menuAction())
        self.menubar.addAction(self.menuFissures.menuAction())
        self.menubar.addAction(self.menuVoid_Trader.menuAction())
        self.Arbitration_label = ScrollLabel(self)
        width = self.frameGeometry().width()
        height = self.frameGeometry().height()
        self.Arbitration_label.setGeometry(100, 20, width-110, height-80)
        self.Arbitration_label.setText("Dies sskadllajdlsajdlalkjsdlsajdlalksdlalskdjlalsdasldkajldskajdlkasdlalkdkaldsadlaslj")
        self.Arbitration_label.setVisible(False)
        self._translate = QCoreApplication.translate
        
        self.writefield=QLineEdit(self)
        self.writefield.move(100,100)


        self.writefield.setVisible(False)
        self.pushButton_14 = QtWidgets.QPushButton(self)
        self.pushButton_14.setObjectName("pushButton_14")
        self.pushButton_14.setText(self._translate("MainWindow", "Suchen"))
        self.pushButton_14.move(100,200)
        self.pushButton_14.setVisible(False)

        self.pushButton_15 = QtWidgets.QPushButton(self)
        self.pushButton_15.setObjectName("pushButton_15")
        self.pushButton_15.setText(self._translate("MainWindow", "Suchen"))
        self.pushButton_15.move(100,200)
        self.pushButton_15.setVisible(False)
        self.timer = QtCore.QTimer()
        self.pushButton_20 = QtWidgets.QPushButton(self)
        self.pushButton_20.setObjectName("pushButton_20")
        self.pushButton_20.setText(self._translate("MainWindow", "Suchen"))
        self.pushButton_20.move(100,200)
        self.pushButton_20.setVisible(False)
        self.timer2 = QtCore.QTimer()
        self.timer2.timeout.connect(self.fissures)
        QtCore.QMetaObject.connectSlotsByName(self)

        self.Fissures_label = ScrollLabel(self)
        self.Arbitration_label_klein = ScrollLabel(self)
        self.Outpost_label = ScrollLabel(self)
        self.Darvo_label = ScrollLabel(self)
        self.Cetus_label = ScrollLabel(self)
        self.info_label = ScrollLabel(self)

        width = self.frameGeometry().width()
        height = self.frameGeometry().height()
        Arbitration_label_klein_width = ((100+((width-130)/3))+10)
        Outpost_label_width = 100+((width-130)/3)+((width-130)/3)+20
        höhe_Reihe_2 = 20+10+((height-90)/2)
        self.Fissures_label.setGeometry(100, 20, ((width-130)/3), ((height-90)/2))
        self.Fissures_label.setText("Dies sskadllajdlsajdlalkjsdlsajdlalksdlalskdjlalsdasldkajldskajdlkasdlalkdkaldsadlaslj")
        self.Arbitration_label_klein.setGeometry(Arbitration_label_klein_width, 20, ((width-130)/3), ((height-90)/2))
        self.Arbitration_label_klein.setText("Dies sskadllajdlsajdlalkjsdlsajdlalksdlalskdjlalsdasldkajldskajdlkasdlalkdkaldsadlaslj")
        self.Outpost_label.setGeometry(Outpost_label_width, 20, ((width-130)/3), ((height-90)/2))
        self.Outpost_label.setText("Dies sskadllajdlsajdlalkjsdlsajdlalksdlalskdjlalsdasldkajldskajdlkasdlalkdkaldsadlaslj")
        self.Darvo_label.setGeometry(100, höhe_Reihe_2, ((width-130)/3), ((height-90)/2))
        self.Darvo_label.setText("Dies sskadllajdlsajdlalkjsdlsajdlalksdlalskdjlalsdasldkajldskajdlkasdlalkdkaldsadlaslj")
        self.Cetus_label.setGeometry(Arbitration_label_klein_width, höhe_Reihe_2, ((width-130)/3), ((height-90)/2))
        self.Cetus_label.setText("Dies sskadllajdlsajdlalkjsdlsajdlalksdlalskdjlalsdasldkajldskajdlkasdlalkdkaldsadlaslj")
        self.info_label.setGeometry(Outpost_label_width, höhe_Reihe_2, ((width-130)/3), ((height-90)/2))
        self.info_label.setText("Dies sskadllajdlsajdlalkjsdlsajdlalksdlalskdjlalsdasldkajldskajdlkasdlalkdkaldsadlaslj")

        #self.Fissures_label.setVisible(False)
        self.alle_label_unsichtbar()
        self.retranslateUi(self)
        self.give_actions()
        self.zeige_bild()
        self.welchertimerläuft = ""
    def alle_label_unsichtbar(self):
        self.Fissures_label.setVisible(False)
        self.Arbitration_label_klein.setVisible(False)
        self.Outpost_label.setVisible(False)
        self.Darvo_label.setVisible(False)
        self.Cetus_label.setVisible(False)
        self.info_label.setVisible(False)
        self.Arbitration_label.setVisible(False)
    def alle_label_sichtbar(self):
        self.Fissures_label.setVisible(True)
        self.Arbitration_label_klein.setVisible(True)
        self.Outpost_label.setVisible(True)
        self.Darvo_label.setVisible(True)
        self.Cetus_label.setVisible(True)
        self.info_label.setVisible(True)
    def mouseMoveEvent(self, e):
        #x = e.x()
        #y = e.y()
        #print("x: "+str(x))
        #print("y: "+str(y))
        self.resize_everything()
        super().mouseMoveEvent(e)
    def resize_everything(self):
        width = self.frameGeometry().width()
        height = self.frameGeometry().height()
        self.Arbitration_label.setGeometry(100, 20, width-110, height-80)
        Arbitration_label_klein_width = ((100+((width-130)/3))+10)
        Outpost_label_width = 100+((width-130)/3)+((width-130)/3)+20
        höhe_Reihe_2 = 20+10+((height-90)/2)
        self.Fissures_label.setGeometry(100, 20, ((width-130)/3), ((height-90)/2))
        self.Arbitration_label_klein.setGeometry(Arbitration_label_klein_width, 20, ((width-130)/3), ((height-90)/2))
        self.Outpost_label.setGeometry(Outpost_label_width, 20, ((width-130)/3), ((height-90)/2))
        self.Darvo_label.setGeometry(100, höhe_Reihe_2, ((width-130)/3), ((height-90)/2))
        self.Cetus_label.setGeometry(Arbitration_label_klein_width, höhe_Reihe_2, ((width-130)/3), ((height-90)/2))
        self.info_label.setGeometry(Outpost_label_width, höhe_Reihe_2, ((width-130)/3), ((height-90)/2))
        
    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        MainWindow.setAccessibleDescription(_translate("MainWindow", "<html><head/><body><p>Hallo</p><p><br/></p></body></html>"))
        self.pushButton_7.setText(_translate("MainWindow", "Start"))
        self.pushButton.setText(_translate("MainWindow", "Arbitrations"))
        self.pushButton_3.setText(_translate("MainWindow", "Void Trader"))
        self.pushButton_4.setText(_translate("MainWindow", "Tradechat"))
        self.pushButton_5.setText(_translate("MainWindow", "Riven Hunter"))
        self.pushButton_6.setText(_translate("MainWindow", "Fissures"))
        self.pushButton_8.setText(_translate("MainWindow", "Riven Preise"))
        self.pushButton_9.setText(_translate("MainWindow", "Waffen Daten"))
        self.pushButton_10.setText(_translate("MainWindow", "Outpost"))
        self.pushButton_11.setText(_translate("MainWindow", "Darvo Deal"))
        self.pushButton_12.setText(_translate("MainWindow", "Item Kaufen"))
        self.pushButton_13.setText(_translate("MainWindow", "Item verkaufen"))
        self.pushButton_16.setText(_translate("MainWindow", "PC Übersicht"))
        self.pushButton_17.setText(_translate("MainWindow", "SWI Übersicht"))
        self.pushButton_18.setText(_translate("MainWindow", "XB1 Übersicht"))
        self.pushButton_21.setText(_translate("MainWindow", "Droptable Reader"))
        self.pushButton_19.setText(_translate("MainWindow", "PS4 Übersicht"))
        self.pushButton_2.setText(_translate("MainWindow", "PushButton"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.menuSettings.setTitle(_translate("MainWindow", "Settings"))
        self.menuArbitrations.setTitle(_translate("MainWindow", "Arbitrations"))
        self.menuFissures.setTitle(_translate("MainWindow", "Fissures"))
        self.menuVoid_Trader.setTitle(_translate("MainWindow", "Void Trader"))
        self.actionExit.setText(_translate("MainWindow", "Exit"))
        self.actionPC.setText(_translate("MainWindow", "PC"))
        self.actionSwitch.setText(_translate("MainWindow", "Switch"))
        self.actionPlaystation_4.setText(_translate("MainWindow", "Playstation 4"))
        self.actionXbox_One.setText(_translate("MainWindow", "Xbox One"))
    def zeige_bild(self):
        self.show()
        
    def give_actions(self):
        self.pushButton.clicked.connect(self.start_arbitration)
        self.pushButton_6.clicked.connect(self.start_fissures)
        self.pushButton_8.clicked.connect(self.riven_prices)
        self.pushButton_14.clicked.connect(self.Eingaben)
        self.pushButton_11.clicked.connect(self.Darvo_Deal)
        self.pushButton_10.clicked.connect(self.Outpost)
        self.pushButton_9.clicked.connect(self.waffen_daten)
        self.pushButton_15.clicked.connect(self.Eingaben2)
        self.pushButton_7.clicked.connect(self.start_function)
        self.pushButton_16.clicked.connect(self.start_pc_all)
        self.pushButton_17.clicked.connect(self.start_swi_all)
        self.pushButton_18.clicked.connect(self.start_xb1_all)
        self.pushButton_19.clicked.connect(self.start_ps4_all)
        self.pushButton_20.clicked.connect(self.Eingaben3)
        self.pushButton_21.clicked.connect(self.droptable)
    def start_pc_all(self):
        self.zeige_alles_pc()
        self.timer.stop()
        if self.welchertimerläuft == "fissures":
            self.timer.timeout.disconnect(self.fissures)
        if self.welchertimerläuft == "Arbitrations":
            self.timer.timeout.disconnect(self.Arbitrations)
        if self.welchertimerläuft == "ps4alles":
            self.timer.timeout.disconnect(self.zeige_alles_ps4)
        if self.welchertimerläuft == "swialles":
            self.timer.timeout.disconnect(self.zeige_alles_swi)
        if self.welchertimerläuft == "xb1alles":
            self.timer.timeout.disconnect(self.zeige_alles_xb1)
        self.timer.timeout.connect(self.zeige_alles_pc)
        self.timer.start(60000)
    def start_swi_all(self):
        self.zeige_alles_swi()
        self.timer.stop()
        if self.welchertimerläuft == "fissures":
            self.timer.timeout.disconnect(self.fissures)
        if self.welchertimerläuft == "Arbitrations":
            self.timer.timeout.disconnect(self.Arbitrations)
        if self.welchertimerläuft == "pcalles":
            self.timer.timeout.disconnect(self.zeige_alles_pc)
        if self.welchertimerläuft == "ps4alles":
            self.timer.timeout.disconnect(self.zeige_alles_ps4)
        if self.welchertimerläuft == "xb1alles":
            self.timer.timeout.disconnect(self.zeige_alles_xb1)
        self.timer.timeout.connect(self.zeige_alles_swi)
        self.timer.start(60000)
    def start_ps4_all(self):
        self.zeige_alles_ps4()
        self.timer.stop()
        if self.welchertimerläuft == "fissures":
            self.timer.timeout.disconnect(self.fissures)
        if self.welchertimerläuft == "Arbitrations":
            self.timer.timeout.disconnect(self.Arbitrations)
        if self.welchertimerläuft == "pcalles":
            self.timer.timeout.disconnect(self.zeige_alles_pc)
        if self.welchertimerläuft == "swialles":
            self.timer.timeout.disconnect(self.zeige_alles_swi)
        if self.welchertimerläuft == "xb1alles":
            self.timer.timeout.disconnect(self.zeige_alles_xb1)
        self.timer.timeout.connect(self.zeige_alles_ps4)
        self.timer.start(60000)
    def start_xb1_all(self):
        self.zeige_alles_xb1()
        self.timer.stop()
        if self.welchertimerläuft == "fissures":
            self.timer.timeout.disconnect(self.fissures)
        if self.welchertimerläuft == "Arbitrations":
            self.timer.timeout.disconnect(self.Arbitrations)
        if self.welchertimerläuft == "pcalles":
            self.timer.timeout.disconnect(self.zeige_alles_pc)
        if self.welchertimerläuft == "swialles":
            self.timer.timeout.disconnect(self.zeige_alles_swi)
        if self.welchertimerläuft == "ps4alles":
            self.timer.timeout.disconnect(self.zeige_alles_ps4)

        self.timer.timeout.connect(self.zeige_alles_xb1)
        self.timer.start(60000)
    def start_function(self):
        self.alle_label_unsichtbar()
    def start_arbitration(self):
        self.Arbitrations()
        self.timer.stop()
        if self.welchertimerläuft == "fissures":
            self.timer.timeout.disconnect(self.fissures)
        if self.welchertimerläuft == "pcalles":
            self.timer.timeout.disconnect(self.zeige_alles_pc)
        if self.welchertimerläuft == "ps4alles":
            self.timer.timeout.disconnect(self.zeige_alles_ps4)
        if self.welchertimerläuft == "swialles":
            self.timer.timeout.disconnect(self.zeige_alles_swi)
        if self.welchertimerläuft == "xb1alles":
            self.timer.timeout.disconnect(self.zeige_alles_xb1)
        self.timer.timeout.connect(self.Arbitrations)
        self.timer.start(60000)
    def start_fissures(self):
        self.fissures()
        self.timer.stop()
        if self.welchertimerläuft == "Arbitrations":
            self.timer.timeout.disconnect(self.Arbitrations)
        if self.welchertimerläuft == "pcalles":
            self.timer.timeout.disconnect(self.zeige_alles_pc)
        if self.welchertimerläuft == "ps4alles":
            self.timer.timeout.disconnect(self.zeige_alles_ps4)
        if self.welchertimerläuft == "swialles":
            self.timer.timeout.disconnect(self.zeige_alles_swi)
        if self.welchertimerläuft == "xb1alles":
            self.timer.timeout.disconnect(self.zeige_alles_xb1)
        self.timer.timeout.connect(self.fissures)
        self.timer.start(60000)

    def waffen_daten(self):
        self.writefield.setVisible(True)
        self.alle_label_unsichtbar()
        self.pushButton_15.setVisible(True)    
    def Eingaben2(self):
        print(self.writefield.text())
        text=FunktionenBot.Weapondata(self.writefield.text())
        self.Arbitration_label.setText(self._translate("MainWindow",text))
        self.writefield.setVisible(False)
        self.Arbitration_label.setVisible(True)
        self.pushButton_15.setVisible(False)
    def Eingaben3(self):
        print(self.writefield.text())
        text=DroptableReader.vollständige_suche(self.writefield.text())
        self.Arbitration_label.setText(self._translate("MainWindow",text))
        self.writefield.setVisible(False)
        self.Arbitration_label.setVisible(True)
        self.pushButton_20.setVisible(False)
        
    def Weapondates(self, Weapon):
        text=FunktionenBot.Weapondata(Weapon)
        self.Arbitration_label.setText(self._translate("MainWindow",text))
        self.Arbitration_label.setVisible(True)
    def Outpost(self):
        text=FunktionenBot.Outpost()                  
        self.Arbitration_label.setText(self._translate("MainWindow",text))
        self.Arbitration_label.setVisible(True)
    def Darvo_Deal(self):
        print("Test")
        text=FunktionenBot.Darvo_Deals()
        self.Arbitration_label.setText(self._translate("MainWindow",text))
        self.Arbitration_label.setVisible(True)
        print("Test2")
    def zeige_alles_pc(self):
        self.alle_label_unsichtbar()
        self.welchertimerläuft = "pcalles"
        text1=  FunktionenBot.get_fissures_PC()
        text2 = FunktionenBot.getArbitrationPC()
        text3 = FunktionenBot.OutpostPC()
        text4 = FunktionenBot.Darvo_PC()
        text5 = FunktionenBot.getWorldstateCetusPC()
        text6 = FunktionenBot.Informationen_anzeigen()
        self.Fissures_label.setText(self._translate("MainWindow",text1))
        self.Arbitration_label_klein.setText(self._translate("MainWindow",text2))
        self.Outpost_label.setText(self._translate("MainWindow",text3))
        self.Darvo_label.setText(self._translate("MainWindow",text4))
        self.Cetus_label.setText(self._translate("MainWindow",text5))
        self.info_label.setText(self._translate("MainWindow",text6))
        self.alle_label_sichtbar()
        print("PC updated")
    def zeige_alles_swi(self):
        self.alle_label_unsichtbar()
        self.welchertimerläuft = "swialles"
        text1=  FunktionenBot.get_fissures_SWI()
        text2 = FunktionenBot.getArbitrationSWI()
        text3 = FunktionenBot.OutpostSWI()
        text4 = FunktionenBot.Darvo_SWI()
        text5 = FunktionenBot.getWorldstateCetusSWI()
        text6 = FunktionenBot.Informationen_anzeigen()
        self.Fissures_label.setText(self._translate("MainWindow",text1))
        self.Arbitration_label_klein.setText(self._translate("MainWindow",text2))
        self.Outpost_label.setText(self._translate("MainWindow",text3))
        self.Darvo_label.setText(self._translate("MainWindow",text4))
        self.Cetus_label.setText(self._translate("MainWindow",text5))
        self.info_label.setText(self._translate("MainWindow",text6))
        self.alle_label_sichtbar()
        print("PC updated")
    def zeige_alles_ps4(self):
        self.alle_label_unsichtbar()
        self.welchertimerläuft = "ps4alles"
        text1=  FunktionenBot.get_fissures_PS4()
        text2 = FunktionenBot.getArbitrationPS4()
        text3 = FunktionenBot.OutpostPS4()
        text4 = FunktionenBot.Darvo_PS4()
        text5 = FunktionenBot.getWorldstateCetusPS4()
        text6 = FunktionenBot.Informationen_anzeigen()
        self.Fissures_label.setText(self._translate("MainWindow",text1))
        self.Arbitration_label_klein.setText(self._translate("MainWindow",text2))
        self.Outpost_label.setText(self._translate("MainWindow",text3))
        self.Darvo_label.setText(self._translate("MainWindow",text4))
        self.Cetus_label.setText(self._translate("MainWindow",text5))
        self.info_label.setText(self._translate("MainWindow",text6))
        self.alle_label_sichtbar()
        print("PC updated")
    def zeige_alles_xb1(self):
        self.alle_label_unsichtbar()
        self.welchertimerläuft = "xb1alles"
        text1=  FunktionenBot.get_fissures_XB1()
        text2 = FunktionenBot.getArbitrationXB1()
        text3 = FunktionenBot.OutpostXB1()
        text4 = FunktionenBot.Darvo_XB1()
        text5 = FunktionenBot.getWorldstateCetusXB1()
        text6 = FunktionenBot.Informationen_anzeigen()
        self.Fissures_label.setText(self._translate("MainWindow",text1))
        self.Arbitration_label_klein.setText(self._translate("MainWindow",text2))
        self.Outpost_label.setText(self._translate("MainWindow",text3))
        self.Darvo_label.setText(self._translate("MainWindow",text4))
        self.Cetus_label.setText(self._translate("MainWindow",text5))
        self.info_label.setText(self._translate("MainWindow",text6))
        self.alle_label_sichtbar()
        print("PC updated")
    def riven_prices(self):
        self.writefield.setVisible(True)
        self.alle_label_unsichtbar()
        self.pushButton_14.setVisible(True)
        print("HI")
    def droptable(self):
        self.writefield.setVisible(True)
        self.alle_label_unsichtbar()
        self.pushButton_20.setVisible(True)
        print("HI")
    def fissures(self):
        self.welchertimerläuft = "fissures"
        text=FunktionenBot.get_all_fissures()
        print("fissures angezeigt")
        self.Arbitration_label.setText(self._translate("MainWindow",text))
        self.Arbitration_label.setVisible(True)
    def Arbitrations(self):
        self.welchertimerläuft="Arbitrations"
        text=FunktionenBot.get_all_arbitrations()
        print("Arbitration angezeigt")
        self.Arbitration_label.setText(self._translate("MainWindow",text))
        self.Arbitration_label.setVisible(True)
    def Eingaben(self):
        print(self.writefield.text())
        text=FunktionenBot.getRivensall(self.writefield.text())
        self.Arbitration_label.setText(self._translate("MainWindow",text))
        self.writefield.setVisible(False)
        self.Arbitration_label.setVisible(True)
        self.pushButton_14.setVisible(False)

        
        
        
        
app = QApplication(sys.argv)

w=WarframeCompanion()

sys.exit(app.exec_())