
import requests
import json
import os

from HtmlGenerator import HtmlGenerator

def Informationen_anzeigen():
    HTML = HtmlGenerator()
    HTML.new_line("Warframe Companion V0.0.4")
    HTML.new_line("written by SpielerNogard with Python")
    HTML.new_line("______________________________________________________________________")
    HTML.new_line("Dieses Programm ist die Umsetzung meiner Discord Bots als eigenständiges System")
    HTML.new_line("Die Informationen werden, jede Minute geupdated. ")
    HTML.new_line("Das Programm verbraucht nur sehr wenige Ressourcen, kann also ohne Probleme im Hintergund laufen")
    HTML.new_line("______________________________________________________________________")
    HTML.new_line("Es enthält im Moment die Folgenden Funktionen:")
    HTML.new_line("- Arbitrationsinfo für alle Konsolen")
    HTML.new_line("- VoidFissures für alle Konsolen")
    HTML.new_line("- Waffendaten zu fast allen Waffen")
    HTML.new_line("- Sentient Outpost aller Konsolen")
    HTML.new_line("- Darvos Deal für alle Konsolen")
    HTML.new_line("- Rivenpreise für ungerollte Rivens")
    HTML.new_line("- Übersicht PC")
    HTML.new_line("- Übersicht XB1")
    HTML.new_line("- Übersicht PS4")
    HTML.new_line("- Übersicht SWI")
    HTML.new_line("- Droptable Reader")
    HTML.new_line("______________________________________________________________________")
    HTML.new_line("Funktionen, die noch folgen")
    HTML.new_line("- Live Tradechat")
    HTML.new_line("- RivenHunter")
    HTML.new_line("- Droptable Reader")
    HTML.new_line("- Automatische Updates")
    HTML.new_line("- Voidtrader")
    HTML.new_line("- Warframe Market integration")
    HTML.new_line("______________________________________________________________________")
    HTML.new_line(" Das Programm befindet sich noch in der Entwicklung, es kann also Bugs enthalten.")
    HTML.new_line(" Sollten Bugs auftreten, meldet euch bitte per Discord bei mir:")
    HTML.new_line("SpielerNogard #8142")
    text = HTML.give_html("1")  
    return(text) 
#gibt die Momentane Zeit aus Cetus zurück (PC)
def getWorldstateCetusPC():
    try:
        HTML = HtmlGenerator()
        HTML.new_line("Cetus Day Night Cycle")
        url='https://api.warframestat.us/pc/cetusCycle/'
        with requests.get(url) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            Out = python_json_obj["shortString"]
        HTML.new_line(Out)
        HTML.new_line("______________________________________________________________________")
        text = HTML.give_html("1")
    except:
        text = "Error while searching for WorldStatus Cetus"
    return(text)


#gibt die Momentane Zeit aus Cetus zurück (SWI)
def getWorldstateCetusSWI():
    try:
        url='https://api.warframestat.us/swi/cetusCycle/'
        with requests.get(url) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            Out = python_json_obj["shortString"]
    except:
        Out = "Error while searching for WorldStatus Cetus"
    return Out

#gibt die Momentane Zeit aus Cetus zurück (XB1)
def getWorldstateCetusXB1():
    try:
        url='https://api.warframestat.us/xb1/cetusCycle/'
        with requests.get(url) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            Out = python_json_obj["shortString"]
    except:
        Out = "Error while searching for WorldStatus Cetus"
    return Out

#gibt die Momentane Zeit aus Cetus zurück (PS4)
def getWorldstateCetusPS4():
    try:
        url='https://api.warframestat.us/ps4/cetusCycle/'
        with requests.get(url) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            Out = python_json_obj["shortString"]
    except:
        Out = "Error while searching for WorldStatus Cetus"
    return Out

#gibt die momentane Arbitration zurück
def getArbitartionDataPC():
    try:
        Out = ""
        Out1 = ""
        Out2 = ""
        Out3 = ""
        Out4 = ""
        url='https://api.warframestat.us/pc/arbitration'
        with requests.get(url) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            Out = python_json_obj["node"]
            Out1 = python_json_obj["enemy"]
            Out2 = python_json_obj["type"]
            Out3 = python_json_obj["activation"] 
            Out4 = python_json_obj["expiry"]
            Arbitration = 'Ort: '+Out+' ('+Out1+') '+Out2
    except:
        Arbitration = "Error while checking Arbitration Status"
    return Arbitration,Out,Out1,Out2,Out3,Out4  

def getArbitartionDataSWI():
    try:
        Out = ""
        Out1 = ""
        Out2 = ""
        Out3 = ""
        Out4 = ""
        url='https://api.warframestat.us/swi/arbitration'
        with requests.get(url) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            Out = python_json_obj["node"]
            Out1 = python_json_obj["enemy"]
            Out2 = python_json_obj["type"]
            Out3 = python_json_obj["activation"] 
            Out4 = python_json_obj["expiry"]
            Arbitration = 'Ort: '+Out+' ('+Out1+') '+Out2
    except:
        Arbitration = "Error while checking Arbitration Status"
    return Arbitration,Out,Out1,Out2,Out3,Out4  

def getArbitartionDataPS4():
    try:
        Out = ""
        Out1 = ""
        Out2 = ""
        Out3 = ""
        Out4 = ""
        url='https://api.warframestat.us/ps4/arbitration'
        with requests.get(url) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            Out = python_json_obj["node"]
            Out1 = python_json_obj["enemy"]
            Out2 = python_json_obj["type"]
            Out3 = python_json_obj["activation"] 
            Out4 = python_json_obj["expiry"]
            Arbitration = 'Ort: '+Out+' ('+Out1+') '+Out2
    except:
        Arbitration = "Error while checking Arbitration Status"
    return Arbitration,Out,Out1,Out2,Out3,Out4 

def getArbitartionDataXB1():
    try:
        Out = ""
        Out1 = ""
        Out2 = ""
        Out3 = ""
        Out4 = ""
        url='https://api.warframestat.us/xb1/arbitration'
        with requests.get(url) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            Out = python_json_obj["node"]
            Out1 = python_json_obj["enemy"]
            Out2 = python_json_obj["type"]
            Out3 = python_json_obj["activation"] 
            Out4 = python_json_obj["expiry"]
            Arbitration = 'Ort: '+Out+' ('+Out1+') '+Out2
    except:
        Arbitration = "Error while checking Arbitration Status"
    return Arbitration,Out,Out1,Out2,Out3,Out4  

def get_fissures_PC():
        HTML = HtmlGenerator()
        HTML.new_line("Derzeit aktive Voidfissures auf dem PC")
        #PC
        url='https://api.warframestat.us/PC/fissures'
        response = requests.get(url, stream=True)
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["node"]
                Out2 = python_json_obj["missionType"]
                Out3 = python_json_obj["enemy"]
                Out5 = python_json_obj["tier"]
                Out4 = python_json_obj["eta"]
                Out = Out5+": "+Out2+" ("+Out3+") "+Out1+" Time left: "+Out4
                print(Out)  
                HTML.new_line(Out)
        


            except:
                info="Fehler in fissures PC"
                print(info)
        HTML.new_line("______________________________________________________________________")
        text = HTML.give_html("1")
        #print(text)
        return(text)

def get_fissures_SWI():
        HTML = HtmlGenerator()
        HTML.new_line("Derzeit aktive Voidfissures auf der Switch")

        url='https://api.warframestat.us/swi/fissures'
        response = requests.get(url, stream=True)
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["node"]
                Out2 = python_json_obj["missionType"]
                Out3 = python_json_obj["enemy"]
                Out5 = python_json_obj["tier"]
                Out4 = python_json_obj["eta"]
                Out = Out5+": "+Out2+" ("+Out3+") "+Out1+" Time left: "+Out4
                print(Out)  
                HTML.new_line(Out)
            except:
                info="Fehler in fissures SWI"
                print(info)
        HTML.new_line("______________________________________________________________________")
        text = HTML.give_html("1")
        #print(text)
        return(text)

def get_fissures_PS4():        
        HTML = HtmlGenerator()
        HTML.new_line("Derzeit aktive Voidfissures auf der Playstation 4")

        url='https://api.warframestat.us/ps4/fissures'
        response = requests.get(url, stream=True)
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["node"]
                Out2 = python_json_obj["missionType"]
                Out3 = python_json_obj["enemy"]
                Out5 = python_json_obj["tier"]
                Out4 = python_json_obj["eta"]
                Out = Out5+": "+Out2+" ("+Out3+") "+Out1+" Time left: "+Out4
                print(Out)  
                HTML.new_line(Out)
            except:
                info="Fehler in fissures SWI"
                print(info)
        HTML.new_line("______________________________________________________________________")
        text = HTML.give_html("1")
        #print(text)
        return(text)
def get_fissures_XB1():        
        HTML = HtmlGenerator()
        HTML.new_line("Derzeit aktive Voidfissures auf der XBOX One")

        url='https://api.warframestat.us/xb1/fissures'
        response = requests.get(url, stream=True)
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["node"]
                Out2 = python_json_obj["missionType"]
                Out3 = python_json_obj["enemy"]
                Out5 = python_json_obj["tier"]
                Out4 = python_json_obj["eta"]
                Out = Out5+": "+Out2+" ("+Out3+") "+Out1+" Time left: "+Out4
                print(Out)  
                HTML.new_line(Out)
            except:
                info="Fehler in fissures SWI"
                print(info)
        HTML.new_line("______________________________________________________________________")
        text = HTML.give_html("1")
        #print(text)
        return(text)      

def get_all_fissures():
    text1 = get_fissures_PC()
    text2 = get_fissures_SWI()
    text3 = get_fissures_PS4()
    text4 = get_fissures_XB1()
    text = text1+text2+text3+text4
    #print(text)
    return(text)
def getArbitrationPC():
    Out1,Node1,Enemy1,Type1,Activation1,Expiry1  = getArbitartionDataPC()
    HTML = HtmlGenerator()
    HTML.new_line("Derzeit aktive arbitration für PC")
        
    HTML.new_line("         Node: "+Node1)
    HTML.new_line("         Enemy: "+Enemy1)
    HTML.new_line("         Type: "+Type1)
    HTML.new_line("         Activation: "+Activation1)
    HTML.new_line("         Expiry: "+Expiry1)
    HTML.new_line("_______________________________________________________________")
    text = HTML.give_html("1")
    return(text)
def getArbitrationSWI():
    Out1,Node1,Enemy1,Type1,Activation1,Expiry1  = getArbitartionDataSWI()
    HTML = HtmlGenerator()
    HTML.new_line("Derzeit aktive arbitration für die SWITCH")
        
    HTML.new_line("         Node: "+Node1)
    HTML.new_line("         Enemy: "+Enemy1)
    HTML.new_line("         Type: "+Type1)
    HTML.new_line("         Activation: "+Activation1)
    HTML.new_line("         Expiry: "+Expiry1)
    HTML.new_line("_______________________________________________________________")
    text = HTML.give_html("1")
    return(text)
def getArbitrationPS4():
    Out1,Node1,Enemy1,Type1,Activation1,Expiry1  = getArbitartionDataPS4()
    HTML = HtmlGenerator()
    HTML.new_line("Derzeit aktive arbitration für die PS4")
        
    HTML.new_line("         Node: "+Node1)
    HTML.new_line("         Enemy: "+Enemy1)
    HTML.new_line("         Type: "+Type1)
    HTML.new_line("         Activation: "+Activation1)
    HTML.new_line("         Expiry: "+Expiry1)
    HTML.new_line("_______________________________________________________________")
    text = HTML.give_html("1")
    return(text)
def getArbitrationXB1():
    Out1,Node1,Enemy1,Type1,Activation1,Expiry1  = getArbitartionDataXB1()
    HTML = HtmlGenerator()
    HTML.new_line("Derzeit aktive arbitration für Die XB1")
        
    HTML.new_line("         Node: "+Node1)
    HTML.new_line("         Enemy: "+Enemy1)
    HTML.new_line("         Type: "+Type1)
    HTML.new_line("         Activation: "+Activation1)
    HTML.new_line("         Expiry: "+Expiry1)
    HTML.new_line("_______________________________________________________________")
    text = HTML.give_html("1")
    return(text)
def get_all_arbitrations():
        Out1,Node1,Enemy1,Type1,Activation1,Expiry1  = getArbitartionDataPC()
        Out2,Node2,Enemy2,Type2,Activation2,Expiry2 = getArbitartionDataSWI() 
        Out3,Node3,Enemy3,Type3,Activation3,Expiry3 = getArbitartionDataPS4() 
        Out4,Node4,Enemy4,Type4,Activation4,Expiry4 = getArbitartionDataXB1() 

        HTML = HtmlGenerator()
        HTML.new_line("Derzeit aktive arbitration für PC")
        
        HTML.new_line("         Node: "+Node1)
        HTML.new_line("         Enemy: "+Enemy1)
        HTML.new_line("         Type: "+Type1)
        HTML.new_line("         Activation: "+Activation1)
        HTML.new_line("         Expiry: "+Expiry1)
        HTML.new_line("_______________________________________________________________")
        HTML.new_line("Derzeit aktive arbitration für SWI")
        
        HTML.new_line("         Node: "+Node2)
        HTML.new_line("         Enemy: "+Enemy2)
        HTML.new_line("         Type: "+Type2)
        HTML.new_line("         Activation: "+Activation2)
        HTML.new_line("         Expiry: "+Expiry2)
        HTML.new_line("_______________________________________________________________")
        HTML.new_line("Derzeit aktive arbitration für PS4")
        
        HTML.new_line("         Node: "+Node3)
        HTML.new_line("         Enemy: "+Enemy3)
        HTML.new_line("         Type: "+Type3)
        HTML.new_line("         Activation: "+Activation3)
        HTML.new_line("         Expiry: "+Expiry3)
        HTML.new_line("_______________________________________________________________")
        HTML.new_line("Derzeit aktive arbitration für XB1")
        
        HTML.new_line("         Node: "+Node4)
        HTML.new_line("         Enemy: "+Enemy4)
        HTML.new_line("         Type: "+Type4)
        HTML.new_line("         Activation: "+Activation4)
        HTML.new_line("         Expiry: "+Expiry4)
        text = HTML.give_html("1")
        return(text)

def getRivensPC(Search):
    HTML = HtmlGenerator()
    HTML.new_line("Hier sind deine Rivenpreise für: "+Search)
    url='https://api.warframestat.us/PC/rivens/search/'+Search
    response = requests.get(url, stream=True)
    for line in response.iter_lines(decode_unicode=True, delimiter="}"):
        try:
            info=line.split("{")[1]
            info2="{"+info+"}"
            python_json_obj = json.loads(info2)
            Out1 = python_json_obj["itemType"]
            Out2 = python_json_obj["compatibility"]
            Out3 = python_json_obj["rerolled"]
            Out5 = python_json_obj["avg"]
            Out4 = python_json_obj["stddev"]
            Out6 = python_json_obj["min"]
            Out7 = python_json_obj["max"]
            Out8 = python_json_obj["pop"]
            Out9 = python_json_obj["median"]
            Out = "Dein gesuchter Riven: "+"\n"+"Riven Art: "+Out1+"\n"+"Waffe: "+Out2+"\n"+"gerollt?: "+str(Out3)+"\n"+"Lowest Price: "+str(Out6)+"\n"+"Highest Price: "+str(Out7)
            print(Out) 



        except:
            info="Fehler"
            print(info)
    HTML.new_line("Plattform: PC")
    HTML.new_line("Riven Art: "+str(Out1))
    HTML.new_line("Waffe: "+str(Out2))
    HTML.new_line("gerollt: "+str(Out3))
    HTML.new_line("stddev: "+str(Out4))
    HTML.new_line("avg: "+str(Out5))
    HTML.new_line("min: "+str(Out6))
    HTML.new_line("max: "+str(Out7))
    HTML.new_line("pop: "+str(Out8))
    HTML.new_line("median: "+str(Out9))
    HTML.new_line("__________________________________________________________________________")
    text = HTML.give_html("1")
    return(text)

def getRivensSWI(Search):
    HTML = HtmlGenerator()
    url='https://api.warframestat.us/swi/rivens/search/'+Search
    response = requests.get(url, stream=True)
    for line in response.iter_lines(decode_unicode=True, delimiter="}"):
        try:
            info=line.split("{")[1]
            info2="{"+info+"}"
            python_json_obj = json.loads(info2)
            Out1 = python_json_obj["itemType"]
            Out2 = python_json_obj["compatibility"]
            Out3 = python_json_obj["rerolled"]
            Out5 = python_json_obj["avg"]
            Out4 = python_json_obj["stddev"]
            Out6 = python_json_obj["min"]
            Out7 = python_json_obj["max"]
            Out8 = python_json_obj["pop"]
            Out9 = python_json_obj["median"]
            Out = "Dein gesuchter Riven: "+"\n"+"Riven Art: "+Out1+"\n"+"Waffe: "+Out2+"\n"+"gerollt?: "+str(Out3)+"\n"+"Lowest Price: "+str(Out6)+"\n"+"Highest Price: "+str(Out7)
            print(Out) 

        except:
            info="Fehler"
            print(info)
    HTML.new_line("Plattform: Switch")
    HTML.new_line("Riven Art: "+str(Out1))
    HTML.new_line("Waffe: "+str(Out2))
    HTML.new_line("gerollt: "+str(Out3))
    HTML.new_line("stddev: "+str(Out4))
    HTML.new_line("avg: "+str(Out5))
    HTML.new_line("min: "+str(Out6))
    HTML.new_line("max: "+str(Out7))
    HTML.new_line("pop: "+str(Out8))
    HTML.new_line("median: "+str(Out9))
    HTML.new_line("__________________________________________________________________________")
    text = HTML.give_html("1")
    return(text)

def getRivensPS4(Search):
    HTML = HtmlGenerator()
    url='https://api.warframestat.us/ps4/rivens/search/'+Search
    response = requests.get(url, stream=True)
    for line in response.iter_lines(decode_unicode=True, delimiter="}"):
        try:
            info=line.split("{")[1]
            info2="{"+info+"}"
            python_json_obj = json.loads(info2)
            Out1 = python_json_obj["itemType"]
            Out2 = python_json_obj["compatibility"]
            Out3 = python_json_obj["rerolled"]
            Out5 = python_json_obj["avg"]
            Out4 = python_json_obj["stddev"]
            Out6 = python_json_obj["min"]
            Out7 = python_json_obj["max"]
            Out8 = python_json_obj["pop"]
            Out9 = python_json_obj["median"]
            Out = "Dein gesuchter Riven: "+"\n"+"Riven Art: "+Out1+"\n"+"Waffe: "+Out2+"\n"+"gerollt?: "+str(Out3)+"\n"+"Lowest Price: "+str(Out6)+"\n"+"Highest Price: "+str(Out7)
            print(Out) 


        except:
            info="Fehler"
            print(info) 
    HTML.new_line("Plattform: Playstation 4")
    HTML.new_line("Riven Art: "+str(Out1))
    HTML.new_line("Waffe: "+str(Out2))
    HTML.new_line("gerollt: "+str(Out3))
    HTML.new_line("stddev: "+str(Out4))
    HTML.new_line("avg: "+str(Out5))
    HTML.new_line("min: "+str(Out6))
    HTML.new_line("max: "+str(Out7))
    HTML.new_line("pop: "+str(Out8))
    HTML.new_line("median: "+str(Out9))
    HTML.new_line("__________________________________________________________________________")
    text = HTML.give_html("1")
    return(text)
def getRivensXB1(Search):
    HTML = HtmlGenerator()
    url='https://api.warframestat.us/xb1/rivens/search/'+Search
    response = requests.get(url, stream=True)
    for line in response.iter_lines(decode_unicode=True, delimiter="}"):
        try:
            info=line.split("{")[1]
            info2="{"+info+"}"
            python_json_obj = json.loads(info2)
            Out1 = python_json_obj["itemType"]
            Out2 = python_json_obj["compatibility"]
            Out3 = python_json_obj["rerolled"]
            Out5 = python_json_obj["avg"]
            Out4 = python_json_obj["stddev"]
            Out6 = python_json_obj["min"]
            Out7 = python_json_obj["max"]
            Out8 = python_json_obj["pop"]
            Out9 = python_json_obj["median"]
            Out = "Dein gesuchter Riven: "+"\n"+"Riven Art: "+Out1+"\n"+"Waffe: "+Out2+"\n"+"gerollt?: "+str(Out3)+"\n"+"Lowest Price: "+str(Out6)+"\n"+"Highest Price: "+str(Out7)
            print(Out) 

        except:
            info="Fehler"
            print(info)
    HTML.new_line("Plattform: XBOX One")
    HTML.new_line("Riven Art: "+str(Out1))
    HTML.new_line("Waffe: "+str(Out2))
    HTML.new_line("gerollt: "+str(Out3))
    HTML.new_line("stddev: "+str(Out4))
    HTML.new_line("avg: "+str(Out5))
    HTML.new_line("min: "+str(Out6))
    HTML.new_line("max: "+str(Out7))
    HTML.new_line("pop: "+str(Out8))
    HTML.new_line("median: "+str(Out9))
    HTML.new_line("__________________________________________________________________________")
    text = HTML.give_html("1")
    return(text)
def getRivensall(Search):
    text1 = getRivensPC(Search)
    text2 = getRivensSWI(Search)
    text3= getRivensPS4(Search)
    text4= getRivensXB1(Search)
    text = text1+text2+text3+text4
    return(text)

def Darvo_PC():
    HTML = HtmlGenerator()
    try:
        url='https://api.warframestat.us/PC/dailyDeals'
        response = requests.get(url, stream=True)
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["item"]
                Out2 = python_json_obj["originalPrice"]
                Out3 = python_json_obj["salePrice"]
                Out4 = python_json_obj["total"]
                Out5 = python_json_obj["sold"]
                Out6 = python_json_obj["eta"]
                
            except:
                print("Fehler in PC Darvo")
        HTML.new_line("Hier sind die aktuellen Darvodeals für den PC")
        HTML.new_line("Item: "+str(Out1))
        HTML.new_line("Original Preis: "+str(Out2))
        HTML.new_line("Reduzierter Preis: "+str(Out3))
        HTML.new_line("Insgesamt vorhanden: "+str(Out4))
        HTML.new_line("bisher verkauft: "+str(Out4))
        HTML.new_line("verbleibend: "+str(Out5))
        HTML.new_line("_________________________________________________________________")
        text = HTML.give_html("1")
        return(text)
    except:
        print("Fehler in Darvo PC")
def Darvo_SWI():
    HTML = HtmlGenerator()
    try:
        url='https://api.warframestat.us/swi/dailyDeals'
        response = requests.get(url, stream=True)
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["item"]
                Out2 = python_json_obj["originalPrice"]
                Out3 = python_json_obj["salePrice"]
                Out4 = python_json_obj["total"]
                Out5 = python_json_obj["sold"]
                Out6 = python_json_obj["eta"]
                
            except:
                print("Fehler in PC Darvo")
        HTML.new_line("Hier sind die aktuellen Darvodeals für die Switch")
        HTML.new_line("Item: "+str(Out1))
        HTML.new_line("Original Preis: "+str(Out2))
        HTML.new_line("Reduzierter Preis: "+str(Out3))
        HTML.new_line("Insgesamt vorhanden: "+str(Out4))
        HTML.new_line("bisher verkauft: "+str(Out4))
        HTML.new_line("verbleibend: "+str(Out5))
        HTML.new_line("_________________________________________________________________")
        text = HTML.give_html("1")
        return(text)
    except:
        print("Fehler in Darvo PC")
def Darvo_PS4():
    HTML = HtmlGenerator()
    try:
        url='https://api.warframestat.us/ps4/dailyDeals'
        response = requests.get(url, stream=True)
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["item"]
                Out2 = python_json_obj["originalPrice"]
                Out3 = python_json_obj["salePrice"]
                Out4 = python_json_obj["total"]
                Out5 = python_json_obj["sold"]
                Out6 = python_json_obj["eta"]
                
            except:
                print("Fehler in PC Darvo")
        HTML.new_line("Hier sind die aktuellen Darvodeals für die PS4")
        HTML.new_line("Item: "+str(Out1))
        HTML.new_line("Original Preis: "+str(Out2))
        HTML.new_line("Reduzierter Preis: "+str(Out3))
        HTML.new_line("Insgesamt vorhanden: "+str(Out4))
        HTML.new_line("bisher verkauft: "+str(Out4))
        HTML.new_line("verbleibend: "+str(Out5))
        HTML.new_line("_________________________________________________________________")
        text = HTML.give_html("1")
        return(text)
    except:
        print("Fehler in Darvo PC")
def Darvo_XB1():
    HTML = HtmlGenerator()
    try:
        url='https://api.warframestat.us/xb1/dailyDeals'
        response = requests.get(url, stream=True)
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["item"]
                Out2 = python_json_obj["originalPrice"]
                Out3 = python_json_obj["salePrice"]
                Out4 = python_json_obj["total"]
                Out5 = python_json_obj["sold"]
                Out6 = python_json_obj["eta"]
                
            except:
                print("Fehler in PC Darvo")
        HTML.new_line("Hier sind die aktuellen Darvodeals für die XB1")
        HTML.new_line("Item: "+str(Out1))
        HTML.new_line("Original Preis: "+str(Out2))
        HTML.new_line("Reduzierter Preis: "+str(Out3))
        HTML.new_line("Insgesamt vorhanden: "+str(Out4))
        HTML.new_line("bisher verkauft: "+str(Out4))
        HTML.new_line("verbleibend: "+str(Out5))
        HTML.new_line("_________________________________________________________________")
        text = HTML.give_html("1")
        return(text)
    except:
        print("Fehler in Darvo PC")
def Darvo_Deals():
    HTML = HtmlGenerator()
    HTML.new_line("Hier sind die aktuellen Darvodeals")
    try:
        url='https://api.warframestat.us/PC/dailyDeals'
        response = requests.get(url, stream=True)
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["item"]
                Out2 = python_json_obj["originalPrice"]
                Out3 = python_json_obj["salePrice"]
                Out4 = python_json_obj["total"]
                Out5 = python_json_obj["sold"]
                Out6 = python_json_obj["eta"]
                
            except:
                print("Fehler in PC Darvo")
        HTML.new_line("Hier sind die aktuellen Darvodeals für den PC")
        HTML.new_line("Item: "+str(Out1))
        HTML.new_line("Original Preis: "+str(Out2))
        HTML.new_line("Reduzierter Preis: "+str(Out3))
        HTML.new_line("Insgesamt vorhanden: "+str(Out4))
        HTML.new_line("bisher verkauft: "+str(Out4))
        HTML.new_line("verbleibend: "+str(Out5))
        HTML.new_line("_________________________________________________________________")
        url='https://api.warframestat.us/swi/dailyDeals'
        response = requests.get(url, stream=True)
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["item"]
                Out2 = python_json_obj["originalPrice"]
                Out3 = python_json_obj["salePrice"]
                Out4 = python_json_obj["total"]
                Out5 = python_json_obj["sold"]
                Out6 = python_json_obj["eta"]
                
            except:
                print("Fehler in SWI Darvo")
        HTML.new_line("Hier sind die aktuellen Darvodeals für die Switch")
        HTML.new_line("Item: "+str(Out1))
        HTML.new_line("Original Preis: "+str(Out2))
        HTML.new_line("Reduzierter Preis: "+str(Out3))
        HTML.new_line("Insgesamt vorhanden: "+str(Out4))
        HTML.new_line("bisher verkauft: "+str(Out4))
        HTML.new_line("verbleibend: "+str(Out5))
        HTML.new_line("_________________________________________________________________")
        url='https://api.warframestat.us/ps4/dailyDeals'
        response = requests.get(url, stream=True)
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["item"]
                Out2 = python_json_obj["originalPrice"]
                Out3 = python_json_obj["salePrice"]
                Out4 = python_json_obj["total"]
                Out5 = python_json_obj["sold"]
                Out6 = python_json_obj["eta"]
               
            except:
                print("Fehler in PS4 Darvo")
        HTML.new_line("Hier sind die aktuellen Darvodeals für die PS4")
        HTML.new_line("Item: "+str(Out1))
        HTML.new_line("Original Preis: "+str(Out2))
        HTML.new_line("Reduzierter Preis: "+str(Out3))
        HTML.new_line("Insgesamt vorhanden: "+str(Out4))
        HTML.new_line("bisher verkauft: "+str(Out4))
        HTML.new_line("verbleibend: "+str(Out5))
        HTML.new_line("_________________________________________________________________")
        url='https://api.warframestat.us/xb1/dailyDeals'
        response = requests.get(url, stream=True)
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["item"]
                Out2 = python_json_obj["originalPrice"]
                Out3 = python_json_obj["salePrice"]
                Out4 = python_json_obj["total"]
                Out5 = python_json_obj["sold"]
                Out6 = python_json_obj["eta"]
                
            except:
                print("Fehler in XB1 Darvo")
        HTML.new_line("Hier sind die aktuellen Darvodeals für die XB1")
        HTML.new_line("Item: "+str(Out1))
        HTML.new_line("Original Preis: "+str(Out2))
        HTML.new_line("Reduzierter Preis: "+str(Out3))
        HTML.new_line("Insgesamt vorhanden: "+str(Out4))
        HTML.new_line("bisher verkauft: "+str(Out4))
        HTML.new_line("verbleibend: "+str(Out5))
        HTML.new_line("_________________________________________________________________")
        print("Darvo updated")
        text = HTML.give_html("1")
        return(text)
    except:
        print("Fehler in Darvo_Deal")
def OutpostPC():
    HTML = HtmlGenerator()
    
    try:
        url1='https://api.warframestat.us/PC/sentientOutposts'
        with requests.get(url1) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            mission1 = python_json_obj["mission"]
            active1 = python_json_obj["active"]  
        mission_1 = json.dumps(mission1)
        python_json_obj1 = json.loads(mission_1)
        node1 = python_json_obj1["node"]
        faction1 = python_json_obj1["faction"]
        art1 = python_json_obj1["type"]
        node1 = node1.replace("'", " ")

        HTML.new_line("Hier sind die aktuellen Sentient Outposts für den PC")
        HTML.new_line("Node: "+str(node1))
        HTML.new_line("Faction: "+str(faction1))
        HTML.new_line("Type: "+str(art1))  
        text = HTML.give_html("1")
        return(text)
    except:
        print("Fehler in Outpost PC")
def OutpostSWI():
    HTML = HtmlGenerator()
    
    try:
        url1='https://api.warframestat.us/swi/sentientOutposts'
        with requests.get(url1) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            mission1 = python_json_obj["mission"]
            active1 = python_json_obj["active"]  
        mission_1 = json.dumps(mission1)
        python_json_obj1 = json.loads(mission_1)
        node1 = python_json_obj1["node"]
        faction1 = python_json_obj1["faction"]
        art1 = python_json_obj1["type"]
        node1 = node1.replace("'", " ")

        HTML.new_line("Hier sind die aktuellen Sentient Outposts für die Switch")
        HTML.new_line("Node: "+str(node1))
        HTML.new_line("Faction: "+str(faction1))
        HTML.new_line("Type: "+str(art1))  
        text = HTML.give_html("1")
        return(text)
    except:
        print("Fehler in Outpost PC")
def OutpostPS4():
    HTML = HtmlGenerator()
    
    try:
        url1='https://api.warframestat.us/ps4/sentientOutposts'
        with requests.get(url1) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            mission1 = python_json_obj["mission"]
            active1 = python_json_obj["active"]  
        mission_1 = json.dumps(mission1)
        python_json_obj1 = json.loads(mission_1)
        node1 = python_json_obj1["node"]
        faction1 = python_json_obj1["faction"]
        art1 = python_json_obj1["type"]
        node1 = node1.replace("'", " ")

        HTML.new_line("Hier sind die aktuellen Sentient Outposts für die PS4")
        HTML.new_line("Node: "+str(node1))
        HTML.new_line("Faction: "+str(faction1))
        HTML.new_line("Type: "+str(art1))  
        text = HTML.give_html("1")
        return(text)
    except:
        print("Fehler in Outpost PC")
def OutpostXB1():
    HTML = HtmlGenerator()
    
    try:
        url1='https://api.warframestat.us/xb1/sentientOutposts'
        with requests.get(url1) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            mission1 = python_json_obj["mission"]
            active1 = python_json_obj["active"]  
        mission_1 = json.dumps(mission1)
        python_json_obj1 = json.loads(mission_1)
        node1 = python_json_obj1["node"]
        faction1 = python_json_obj1["faction"]
        art1 = python_json_obj1["type"]
        node1 = node1.replace("'", " ")

        HTML.new_line("Hier sind die aktuellen Sentient Outposts für die XB1")
        HTML.new_line("Node: "+str(node1))
        HTML.new_line("Faction: "+str(faction1))
        HTML.new_line("Type: "+str(art1))  
        text = HTML.give_html("1")
        return(text)
    except:
        print("Fehler in Outpost PC")
def Outpost():
    HTML = HtmlGenerator()
    HTML.new_line("Hier sind die aktuellen Sentient Outposts")
    try:
        url1='https://api.warframestat.us/PC/sentientOutposts'
        with requests.get(url1) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            mission1 = python_json_obj["mission"]
            active1 = python_json_obj["active"]


        url2='https://api.warframestat.us/swi/sentientOutposts'
        with requests.get(url2) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            mission2 = python_json_obj["mission"]
            active2 = python_json_obj["active"]

        
        url3='https://api.warframestat.us/ps4/sentientOutposts'
        with requests.get(url3) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            mission3 = python_json_obj["mission"]
            active3 = python_json_obj["active"]            

            
        url4='https://api.warframestat.us/xb1/sentientOutposts'
        with requests.get(url4) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            mission4 = python_json_obj["mission"]
            active4 = python_json_obj["active"]

        #print("PC-Outpost: "+str(mission1)+" is active: "+str(active1))
        #print("SWI-Outpost: "+str(mission2)+" is active: "+str(active2))
        #print("PS4-Outpost: "+str(mission3)+" is active: "+str(active3))
        #print("XB1-Outpost: "+str(mission4)+" is active: "+str(active4))

        mission_1 = json.dumps(mission1)
        mission_2 = json.dumps(mission2)
        mission_3 = json.dumps(mission3)
        mission_4 = json.dumps(mission4)

        python_json_obj1 = json.loads(mission_1)
        python_json_obj2 = json.loads(mission_2)
        python_json_obj3 = json.loads(mission_3)
        python_json_obj4 = json.loads(mission_4)

        node1 = python_json_obj1["node"]
        node2 = python_json_obj2["node"]
        node3 = python_json_obj3["node"]
        node4 = python_json_obj4["node"]

        faction1 = python_json_obj1["faction"]
        faction2 = python_json_obj2["faction"]
        faction3 = python_json_obj3["faction"]
        faction4 = python_json_obj4["faction"]

        art1 = python_json_obj1["type"]
        art2 = python_json_obj2["type"]
        art3 = python_json_obj3["type"]
        art4 = python_json_obj4["type"]

        node1 = node1.replace("'", " ")
        node2 = node2.replace("'", " ")
        node3 = node3.replace("'", " ")
        node4 = node4.replace("'", " ")
        HTML.new_line("Hier sind die aktuellen Sentient Outposts für den PC")
        HTML.new_line("Node: "+str(node1))
        HTML.new_line("Faction: "+str(faction1))
        HTML.new_line("Type: "+str(art1))
        HTML.new_line("________________________________________________________________")
        HTML.new_line("Hier sind die aktuellen Sentient Outposts für die Switch")
        HTML.new_line("Node: "+str(node2))
        HTML.new_line("Faction: "+str(faction2))
        HTML.new_line("Type: "+str(art2))
        HTML.new_line("________________________________________________________________")
        HTML.new_line("Hier sind die aktuellen Sentient Outposts für die PS4")
        HTML.new_line("Node: "+str(node3))
        HTML.new_line("Faction: "+str(faction3))
        HTML.new_line("Type: "+str(art3))
        HTML.new_line("________________________________________________________________")
        HTML.new_line("Hier sind die aktuellen Sentient Outposts für die XB1")
        HTML.new_line("Node: "+str(node4))
        HTML.new_line("Faction: "+str(faction4))
        HTML.new_line("Type: "+str(art4))
        HTML.new_line("________________________________________________________________")
        text = HTML.give_html("1")
        return(text)
   


    except:
        info="Fehler in Outpost"
        print(info)  
def WeaponCategory(Weapon):
    try:
        url='https://api.warframestat.us/weapons/'+Weapon
        with requests.get(url) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            Category = python_json_obj["category"]
    except:
        print("Konnte Kategorie nicht feststellen")  
        Category = "Konnte Kategorie nicht feststellen"
    return(Category)

def Weapondata(Weapon):
    HTML = HtmlGenerator()
    
    try:
        Category = WeaponCategory(Weapon)
        print(Category)
        if Category == "Secondary":
            url='https://api.warframestat.us/weapons/'+Weapon
            with requests.get(url) as response:
                #Json Object erhalten
                python_json_obj = response.text
                python_json_obj = json.loads(python_json_obj)
                #Json Object auslesen
                Name = python_json_obj["name"]
                Beschreibung = python_json_obj["description"]
                Bild = python_json_obj["wikiaThumbnail"]
                MagazinGröße = python_json_obj["magazineSize"]
                Nachladezeit = python_json_obj["reloadTime"]
                CritChance = python_json_obj["criticalChance"]
                CritMultiplier = python_json_obj["criticalMultiplier"]
                Damage = python_json_obj["damage"]
                Damageverteilung = python_json_obj["damageTypes"]
                Firerate = python_json_obj["fireRate"]
                Statuschance = python_json_obj["procChance"]
                Mastery = python_json_obj["masteryReq"]
                Riven = python_json_obj["disposition"]
                Noise = python_json_obj["noise"]
                Link = python_json_obj["wikiaUrl"]
                #Ausgabe
            HTML.new_line("Hier ist deine Waffe: ")
            HTML.new_line("Name: "+str(Name))
            HTML.new_line("Beschreibung: "+str(Beschreibung))
            HTML.new_line("Magazingröße: "+str(MagazinGröße))
            HTML.new_line("Nachladezeit: "+str(Nachladezeit))
            HTML.new_line("CritChance: "+str(CritChance))
            HTML.new_line("CritMultiplier: "+str(CritMultiplier))
            HTML.new_line("Damage: "+str(Damage))
            HTML.new_line("Damageverteilung: "+str(Damageverteilung))
            HTML.new_line("Firerate: "+str(Firerate))
            HTML.new_line("Statuschance: "+str(Statuschance))
            HTML.new_line("Mastery: "+str(Mastery))
            HTML.new_line("Riven: "+str(Riven))
            HTML.new_line("Noise: "+str(Noise))
            HTML.new_line("Link: "+str(Link))
            HTML.new_line("________________________________________________________________")
            text = HTML.give_html("1")

                
        if Category == "Melee":
            url='https://api.warframestat.us/weapons/'+Weapon
            with requests.get(url) as response:
                #Json Object erhalten
                python_json_obj = response.text
                python_json_obj = json.loads(python_json_obj)
                #Json Object auslesen
                Name = python_json_obj["name"]
                print(Name)
                Beschreibung = python_json_obj["description"]
                print(Beschreibung)
                Attackspeed = python_json_obj["fireRate"]
                print(Attackspeed)
                CritChance = python_json_obj["criticalChance"]
                print(CritChance)
                CritMultiplier = python_json_obj["criticalMultiplier"]
                print(CritMultiplier)
                Damage = python_json_obj["damage"]
                print(Damage)
                Damageverteilung = python_json_obj["damageTypes"]
                print(Damageverteilung)
                Firerate = python_json_obj["fireRate"]
                print(Firerate)
                Statuschance = python_json_obj["procChance"]
                print(Statuschance)
                Mastery = python_json_obj["masteryReq"]
                print(Mastery)
                Riven = python_json_obj["disposition"]
                print(Riven)
                Spin = python_json_obj["slamAttack"]
                print(Spin)
                Leap = python_json_obj["slideAttack"]
                print(Leap)
                Wall = python_json_obj["heavySlamAttack"]
                print(Wall)
                Link = python_json_obj["wikiaUrl"]
                print(Link)
                Typ = python_json_obj["type"]
                print(Typ)
                
            HTML.new_line("Hier ist deine Waffe: ")
            HTML.new_line("Name: "+str(Name))
            HTML.new_line("Beschreibung: "+str(Beschreibung))
            HTML.new_line("Attackspeed: "+str(Attackspeed))
            HTML.new_line("CritChance: "+str(CritChance))
            HTML.new_line("CritMultiplier: "+str(CritMultiplier))
            HTML.new_line("Damage: "+str(Damage))
            HTML.new_line("Damageverteilung: "+str(Damageverteilung))
            HTML.new_line("Firerate: "+str(Firerate))
            HTML.new_line("Statuschance: "+str(Statuschance))
            HTML.new_line("Mastery: "+str(Mastery))
            HTML.new_line("Riven: "+str(Riven))
            HTML.new_line("Slam Attack: "+str(Spin))
            HTML.new_line("Slide Attack: "+str(Leap))
            HTML.new_line("Heavy slam Attack: "+str(Wall))
            HTML.new_line("Link: "+str(Link))
            HTML.new_line("Typ: "+str(Typ))
            HTML.new_line("________________________________________________________________")
            text = HTML.give_html("1")
                
        if Category == "Primary":
            url='https://api.warframestat.us/weapons/'+Weapon
            with requests.get(url) as response:
                #Json Object erhalten
                python_json_obj = response.text
                python_json_obj = json.loads(python_json_obj)
                #Json Object auslesen
                Name = python_json_obj["name"]
                Beschreibung = python_json_obj["description"]
                Bild = python_json_obj["wikiaThumbnail"]
                MagazinGröße = python_json_obj["magazineSize"]
                Nachladezeit = python_json_obj["reloadTime"]
                CritChance = python_json_obj["criticalChance"]
                CritMultiplier = python_json_obj["criticalMultiplier"]
                Damage = python_json_obj["damage"]
                Damageverteilung = python_json_obj["damageTypes"]
                Firerate = python_json_obj["fireRate"]
                Statuschance = python_json_obj["procChance"]
                Mastery = python_json_obj["masteryReq"]
                Riven = python_json_obj["disposition"]
                Noise = python_json_obj["noise"]
                Link = python_json_obj["wikiaUrl"]
            HTML.new_line("Hier ist deine Waffe: ")
            HTML.new_line("Name: "+str(Name))
            HTML.new_line("Beschreibung: "+str(Beschreibung))
            HTML.new_line("Magazingröße: "+str(MagazinGröße))
            HTML.new_line("Nachladezeit: "+str(Nachladezeit))
            HTML.new_line("CritChance: "+str(CritChance))
            HTML.new_line("CritMultiplier: "+str(CritMultiplier))
            HTML.new_line("Damage: "+str(Damage))
            HTML.new_line("Damageverteilung: "+str(Damageverteilung))
            HTML.new_line("Firerate: "+str(Firerate))
            HTML.new_line("Statuschance: "+str(Statuschance))
            HTML.new_line("Mastery: "+str(Mastery))
            HTML.new_line("Riven: "+str(Riven))
            HTML.new_line("Noise: "+str(Noise))
            HTML.new_line("Link: "+str(Link))
            HTML.new_line("________________________________________________________________")
            text = HTML.give_html("1")
        return(text)
    except:
        print("Fehler in Weapon")  
        
        